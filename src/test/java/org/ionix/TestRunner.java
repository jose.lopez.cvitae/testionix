package org.ionix;

import io.appium.java_client.AppiumBy;
import org.ionix.resources.BaseTest;
import org.testng.Assert;
import org.testng.annotations.Test;


public class TestRunner extends BaseTest {

    @Test
    public void userNameExist() {

        driver.findElements(AppiumBy.className("android.widget.EditText")).get(0).sendKeys("ionixtester");
        driver.findElement(AppiumBy.className("android.widget.Button")).click();
        String alertTxtUsername = driver.findElements(AppiumBy.className("android.widget.TextView")).get(1).getText();

        Assert.assertEquals(alertTxtUsername, "Username exist");

    }
    @Test
    public void usernameWithoutArroba(){

        driver.findElements(AppiumBy.className("android.widget.EditText")).get(0).sendKeys("@");
        driver.findElement(AppiumBy.className("android.widget.Button")).click();
        String alertTxtUsername = driver.findElements(AppiumBy.className("android.widget.TextView")).get(1).getText();


        Assert.assertEquals(alertTxtUsername, "Username should not contain @");

    }
    @Test
    public void userNameEmpty() {

        driver.findElements(AppiumBy.className("android.widget.EditText")).get(0).sendKeys(" "); // Se envia vacío
        driver.findElement(AppiumBy.className("android.widget.Button")).click();
        String alertTxtUsername = driver.findElements(AppiumBy.className("android.widget.TextView")).get(1).getText();

        Assert.assertEquals(alertTxtUsername, "Username is required");

    }
    @Test
    public void password8characters() {

        driver.findElements(AppiumBy.className("android.widget.EditText")).get(1).sendKeys("asdf"); // Se envian solo 4 caracteres
        driver.findElement(AppiumBy.className("android.widget.Button")).click();
        String alertTxtPassword = driver.findElements(AppiumBy.className("android.widget.TextView")).get(1).getText();

        Assert.assertEquals(alertTxtPassword, "Password does not have the format");

    }
    @Test
    public void password1Mayus() {

        driver.findElements(AppiumBy.className("android.widget.EditText")).get(1).sendKeys("p4ssword!"); // Todo en minus sin enviar una mayuscula
        driver.findElement(AppiumBy.className("android.widget.Button")).click();
        String alertTxtPassword = driver.findElements(AppiumBy.className("android.widget.TextView")).get(1).getText();

        Assert.assertEquals(alertTxtPassword, "Password does not have the format");

    }
    @Test
    public void password1Minus() {

        driver.findElements(AppiumBy.className("android.widget.EditText")).get(1).sendKeys("P4SSWORD!"); // Todo en mayus sin enviar una minuscula
        driver.findElement(AppiumBy.className("android.widget.Button")).click();
        String alertTxtPassword = driver.findElements(AppiumBy.className("android.widget.TextView")).get(1).getText();

        Assert.assertEquals(alertTxtPassword, "Password does not have the format");

    }
    @Test
    public void password1Number() {

        driver.findElements(AppiumBy.className("android.widget.EditText")).get(1).sendKeys("password!"); // Se envía el formato correcto sin enviar un numero
        driver.findElement(AppiumBy.className("android.widget.Button")).click();
        String alertTxtPassword = driver.findElements(AppiumBy.className("android.widget.TextView")).get(1).getText();

        Assert.assertEquals(alertTxtPassword, "Password does not have the format");

    }
    @Test
    public void password1SpecialCharacter() {

        driver.findElements(AppiumBy.className("android.widget.EditText")).get(1).sendKeys("password1"); // Se envía el formato correcto sin enviar un caracter especial
        driver.findElement(AppiumBy.className("android.widget.Button")).click();
        String alertTxtPassword = driver.findElements(AppiumBy.className("android.widget.TextView")).get(1).getText();

        Assert.assertEquals(alertTxtPassword, "Password does not have the format");

    }
    @Test
    public void passwordEmpty() {

        driver.findElements(AppiumBy.className("android.widget.EditText")).get(1).sendKeys(""); // Se vacío
        driver.findElement(AppiumBy.className("android.widget.Button")).click();
        String alertTxtPassword = driver.findElements(AppiumBy.className("android.widget.TextView")).get(1).getText();

        Assert.assertEquals(alertTxtPassword, "Password is required");

    }
    @Test
    public void repeatPasswordSameBefore() {

        driver.findElements(AppiumBy.className("android.widget.EditText")).get(1).sendKeys("Password@1.");
        driver.findElements(AppiumBy.className("android.widget.EditText")).get(2).sendKeys("");
        driver.findElement(AppiumBy.className("android.widget.Button")).click();
        String alertTxtRepeatPassword = driver.findElements(AppiumBy.className("android.widget.TextView")).get(2).getText();

        Assert.assertEquals(alertTxtRepeatPassword, "Password should be the same");

    }
    @Test
    public void repeatPasswordEmpty() {

        driver.findElements(AppiumBy.className("android.widget.EditText")).get(2).sendKeys(""); // Se vacío
        driver.findElement(AppiumBy.className("android.widget.Button")).click();
        String alertTxtRepeatPassword = driver.findElements(AppiumBy.className("android.widget.TextView")).get(2).getText();

        Assert.assertEquals(alertTxtRepeatPassword, "Repeat password is required");

    }
    @Test
    public void formatEmail() {

        driver.findElements(AppiumBy.className("android.widget.EditText")).get(3).sendKeys("aaa.com"); // Se vacío
        driver.findElement(AppiumBy.className("android.widget.Button")).click();
        String alertTxtEmail = driver.findElements(AppiumBy.className("android.widget.TextView")).get(3).getText();

        Assert.assertEquals(alertTxtEmail, "Please enter a valid email");

    }
    @Test
    public void emailAlert() {

        driver.findElements(AppiumBy.className("android.widget.EditText")).get(3).sendKeys(""); // Se vacío
        driver.findElement(AppiumBy.className("android.widget.Button")).click();
        String alertTxtEmail = driver.findElements(AppiumBy.className("android.widget.TextView")).get(3).getText();

        Assert.assertEquals(alertTxtEmail, "Do you want to continue without email");

    }

}
